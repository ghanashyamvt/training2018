
# coding: utf-8

# In[70]:


import pandas as pd


# In[188]:


seasondf = pd.read_csv('C:\\Users\\Parphul\\Desktop\\Training\\ipl_data_sets\\Season.csv'


# In[189]:


season_orangedf = seasondf.iloc[:,[0,2]]
season_orangedf.columns=['Season_Id','Player_Id']


# In[162]:


season_purpledf = seasondf.iloc[:,[0,3]]
season_purpledf.columns=['Season_Id','Player_Id']


# In[163]:


season_mosdf = seasondf.iloc[:,[0,4]]
season_mosdf.columns=['Season_Id','Player_Id']


# In[164]:


match_player_teamdf = pd.read_csv("C:\\Users\\Parphul\\Desktop\\Training\\ipl_data_sets\\Player_Match.csv").iloc[:,[0,1,2]]


# In[165]:


match_seasondf = pd.read_csv("C:\\Users\\Parphul\\Desktop\\Training\\ipl_data_sets\\Match.csv").iloc[:,[0,4]]


# In[213]:


season_match_player_teamdf = pd.merge(match_seasondf, match_player_teamdf, on='Match_Id')


# In[233]:


orangecap_team = pd.merge(season_match_player_teamdf,season_orangedf, on =['Season_Id','Player_Id']).iloc[:,[1,3]].drop_duplicates()
orangecap_team1 = orangecap_team.groupby(['Team_Id'],as_index=False).count()
orangecap_team1.columns=['Team_Id','Orange_Caps']


# In[234]:


purplecap_team = pd.merge(season_match_player_teamdf,season_purpledf, on =['Season_Id','Player_Id']).iloc[:,[1,3]].drop_duplicates()
purplecap_team1 = purplecap_team.groupby(['Team_Id'],as_index=False).count()
purplecap_team1.columns=['Team_Id','Purple_Cap']


# In[235]:


mos_team = pd.merge(season_match_player_teamdf,season_mosdf, on =['Season_Id','Player_Id']).iloc[:,[1,3]].drop_duplicates()
mos_team1 = purplecap_team.groupby(['Team_Id'],as_index=False).count()
mos_team1.columns = ['Team_Id','No_Of_Mos']


# In[236]:


teamdf = pd.read_csv('C:\\Users\\Parphul\\Desktop\\Training\\ipl_data_sets\\Team.csv')


# In[243]:


team_orangedf = pd.merge(teamdf,orangecap_team1, how='left', on =['Team_Id'])
team_orange_purpledf = pd.merge(team_orangedf, orangecap_team1, how='left', on=['Team_Id'])
team_orange_purple_mosdf = pd.merge(team_orange_purpledf, mos_team1, how='left', on=['Team_Id'])
final_dataframe = team_orange_purple_mosdf
final_dataframe.columns = ['Team_Id','Team_Name','Team_Short_Code','Orange_Caps','Orange_Caps','No_Of_Mos']
final_dataframe
