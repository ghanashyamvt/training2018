import pandas as pd
def avg_no_of_extras(total_no_of_matches):
	ball_to_ball=pd.read_csv("data/Ball_by_Ball.csv",)
	print("Total number of balls played : "+str(len(ball_to_ball["Extra_Type"].values)))	
	legbyes=sum(1 if ball_to_ball['Extra_Type'].values[i]=='legbyes' else 0 for i in range(len(ball_to_ball['Extra_Type'].values)))
	wides=sum([1 if ball_to_ball["Extra_Type"].values[i]=='wides' else 0 for i in range(len(ball_to_ball["Extra_Type"].values))])
	print("Total number of Wides for all matches :"+str(wides))
        print("Average no of wides per match : "+str((.0+wides)/total_no_of_matches))
	print("Total number of legbyes for all matches :"+str(legbyes))
        print("Average no of legbyes per match : "+str((legbyes+.0)/total_no_of_matches))
def read_my_file(file_name):
	my_file= pd.read_csv(file_name+".csv")	
	print("Total number of matches played till season "+str(max(my_file["Season_Id"].values))+" : "+str	(len(my_file)))
	avg_no_of_extras(len(my_file))
read_my_file('data/Match')

